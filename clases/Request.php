<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of Request
 *
 * @author Schukin Aleksander
 * @version 0.1
 */
class Request {
    private $inputData=[];
    public function __construct(){
        $this->inputData = json_decode(file_get_contents('php://input'), true);
    }

    public function getString($key, $defaultValue = ''):string{
        if (array_key_exists($key, $this->inputData) && is_string($this->inputData[$key])){
            return $this->inputData[$key];
        }else{
            return $defaultValue;
        }
    }
    public function getInt($key, $defaultValue = 0):int{
        if (array_key_exists($key, $this->inputData) && is_integer($this->inputData[$key])){
            return (int)$this->inputData[$key];
        }else{
            return $defaultValue;
        }
    }
    public function getDouble($key, $defaultValue = 0):float{
        if (array_key_exists($key, $this->inputData) && is_float($this->inputData[$key])){
            return (float)$this->inputData[$key];
        }else{
            return $defaultValue;
        }
    }
    public function keyExist($key):bool{
        return array_key_exists($key, $this->inputData);
    }
}
