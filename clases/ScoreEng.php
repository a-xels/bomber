<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

/**
 * Description of ScoreEng
 *
 * @author Schukin Aleksander
 * @version 0.1
 */
require_once __DIR__.'/Request.php';
class ScoreEng {
    public $request;
    public $fName=__DIR__.'/score/score.json';
    public function __construct(){
        $this->request = new Request();
    }
    private function doAction():array{
        $rVal = ['status'=>'ok', 'answer'=>''];
        switch($this->request->getString('action')){
            case 'get-list':
                $rVal['answer']=$this->getList();
                break;
            case 'store-player':
                if ($this->request->getString('pName') && $this->request->keyExist('score')){
                    $rVal['answer']=$this->storePlayer();
                }else{
                    $rVal['answer']=[
                        'errorText'=>'Не указано имя игрока или его счет.'
                    ];
                    $rVal['status']='error';                                    
                }
                break;
            default:
                $rVal['answer']=[
                    'errorText'=>'Не указан или неправильный экшен.'
                ];
                $rVal['status']='error';                
                break;
        }
        return $rVal;
    }
    private function storePlayer():array{
        $rVal=['message'=>'Ничего не изменилось.'];
        $pName = $this->request->getString('pName');
        $pScore = $this->request->getInt('score');
        $tmp = $this->getList();
        if ( ( $index = array_search( $pName, array_column( $tmp, 'name' ) ) ) !== false ){
            if ( $tmp[$index]['score'] < $pScore ){
                $tmp[$index]['score'] = $pScore;
                file_put_contents($this->fName, json_encode($tmp));
            }
            $rVal['message'] = 'Данные игрока обновлены';
        }else{
            
        }
        return $rVal;;
    }
    private function getList():array{
        $rVal = [];
        if (file_exists($this->fName)){
            $rVal = json_decode(file_get_contents( $this->fName ), true);
        }else{
            $rVal = [
                ['name'=>'Player 1','score' => 1250],
                ['name'=>'Player 2','score' => 1500],
                ['name'=>'Player 3','score' => 1750],
                ['name'=>'Player 4','score' => 250],
                ['name'=>'Player 5','score' => 1000],
                ['name'=>'Player 6','score' => 500],
                ['name'=>'Player 7','score' => 750],
            ];
            file_put_contents($this->fName, json_encode($rVal));
        }
        return $rVal;
    }
    public function run():string{
        return json_encode($this->doAction());
    }
}
