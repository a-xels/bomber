/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import { Component } from './component.js';
import { Bilding } from './bilding.js';
import { getRandomInRange }from './blow.js';

export const City = function(options){
    options = options || {};
    if (options.hasOwnProperty('canvasId')){
        this.context2d = document.getElementById(options.canvasId).getContext("2d");
    }else{
        console.error((this.name?this.name:"Bilding") + ": не передан \"canvasId\" canvas");
        return this;       
    }
    Component.call( this, options, {
        fileNameList:[],
        defaultWidth:130,
        onLoadProgress:null,
        onImgLoadProgress:null,
        onLoad:null,
        canvasId:'',
        iManager:null
    }, true, ['fileNameList', 'iManager']);
    this.buldingList = [];
    this.loadTotal = 0;
    return this;
};
City.prototype=Object.create(Component.prototype);
Object.defineProperty(City.prototype, 'constructor', { 
    value: City, 
    enumerable: false, // false, чтобы данное свойство не появлялось в цикле for in
    writable: true 
});
City.prototype.clearPort = function(){
    this.context2d.clearRect( 0, 0, this.context2d.canvas.width, this.context2d.canvas.height );
};
City.prototype.prepareRender = function( widthPercent, height ){
    let imgCounter = 0;
    if (!widthPercent){
        console.error("City: не указан или не верное значение параметра \"widthPercent\"");
        return;
    }
    if (!height){
        console.error("City: не указан или не верное значение параметра \"height\"");
        return;
    }
    const aWidth = Math.floor( this.context2d.canvas.width / 100 * widthPercent );
    let startX = Math.floor(this.context2d.canvas.width / 2 - aWidth / 2);
    const endX = startX + aWidth;
    let loadCounter = 0;
    this.loadTotal = 0;
    if (!this.fileNameList.length){
        console.warn("City: fileNameList - не задан. Нечего выводить!");
        return;
    }
    do{
        const temp = getRandomInRange( 0, this.fileNameList.length - 1 );
        loadCounter++;
        this.loadTotal++;
        imgCounter += this.fileNameList[ temp ].mainFiles.length;
        this.buldingList.push(new Bilding( {
            canvasId:this.canvasId,
            mainFiles:this.fileNameList[ temp ].mainFiles,
            mainSpriteXShift:this.fileNameList[ temp ].mainSpriteXShift?this.fileNameList[ temp ].mainSpriteXShift:[],
            outW:this.fileNameList[ temp ].outW?this.fileNameList[ temp ].outW:this.defaultWidth,
            startX:startX,
            height:getRandomInRange( 0, height ),
            iManager:this.iManager,
            onLoadProgress:(el, t, l) => {
                if (typeof this.onImgLoadProgress === "function"){
                    this.onImgLoadProgress.call(this,t,l);
                }
            },
            onLoad:() => {
                loadCounter--;
                if ( typeof this.onLoadProgress === 'function' ){
                    this.onLoadProgress.call( this, this.loadTotal, loadCounter);
                }else{
                    console.log("City: - осталось загрузить " + loadCounter);
                }
                if (!loadCounter){
                    if (typeof this.onLoad === "function"){
                        this.onLoad.call(this);
                    }else{
                        console.log("City: загрузки завершены. Рендер по умолчанию.");
                        this.renderAll();
                    }
                }
            }
        } ));
        this.loadTotal += this.buldingList[ this.buldingList.length - 1 ].loadTotal;
        startX += this.fileNameList[ temp ].outW?this.fileNameList[ temp ].outW:this.defaultWidth;
        startX += 5;
    }while( startX <  endX);
    return imgCounter;
};
City.prototype.renderAll = function(){
    for (let i = 0;i < this.buldingList.length;i++){
        this.buldingList[i].renderBulding();
    }
};
City.prototype.checkContactTopArea = function (pos){
    let rVal = null;
    for (let i = 0; i < this.buldingList.length && rVal === null; i++){
        if (this.buldingList[i].areas.length){
            const rect = this.buldingList[i].areas[this.buldingList[i].areas.length - 1];
            if ( pos.X > rect.x && pos.X < rect.x + rect.w &&
                 (pos.Y > rect.y && pos.Y < rect.y + rect.h ||
                  pos.Y + 2 > rect.y && pos.Y < rect.y + rect.h ||
                  pos.Y + 4> rect.y && pos.Y < rect.y + rect.h)){
                    rVal = i;
            }
        }
    }
    return rVal; //Возврощает номер здания в крышу которого попало
};
City.prototype.getTopAreaCenterBottomPoint = function( buildingNumber ){
    const rect = this.buldingList[ buildingNumber ].areas[this.buldingList[ buildingNumber ].areas.length - 1];
    return { X: Math.floor(rect.x + rect.w/2), Y: Math.floor(rect.y + rect.h)};
};
City.prototype.removeTopFloor = function( buildingNumber ){
    const rect = this.buldingList[ buildingNumber ].areas.pop();
    this.context2d.clearRect( rect.x, rect.y, rect.w, rect.h );
};

City.prototype.checkContactTopAny = function (pos){
    let rVal = null;
    for (let i = 0; i < this.buldingList.length && rVal === null; i++){
        for (let n = 0; n < this.buldingList[i].areas.length && rVal === null; n++){
            if (this.buldingList[i].areas.length){
                const rect = this.buldingList[i].areas[n];
                if ( pos.X > rect.x && pos.X < rect.x + rect.w &&
                     (pos.Y > rect.y && pos.Y < rect.y + rect.h ||
                      pos.Y + 2 > rect.y && pos.Y < rect.y + rect.h ||
                      pos.Y + 4> rect.y && pos.Y < rect.y + rect.h)){
                        rVal = {buldingNumber:i, areaNumber:n};
                }
            }
        }
    }
    return rVal; //Возврощает номер здания в крышу которого попало    
};
City.prototype.isAllBuldingDestroy = function(){
    let rVal = true;
    for (let i = 0; i < this.buldingList.length && rVal; i++){
        rVal = this.buldingList[i].areas.length === 0;
    }
    return rVal;
};