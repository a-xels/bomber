/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import { blowAnimate as _blA, getRandomInRange }from './blow.js';
_blA.flame=function(options){
    options=options || {};
    const _system = {
            airP : .029,       //Плотность воздуха
            k    : .024,       //Геометрический коофициент сопр
            tt   : .0065,       //Шаг итерации
            g    : 9.8         //Притяжение
    };
    this.blowsProcesing=[];
    this.interval=0;
    this.pieces=[];
    const {
        id:_canvasID =  "",             //ID - элемента canvas
        idMidle:_canvasIDMidle =  "",   //ID - элемента canvas по середине
        img                             //Изображение обект (content:img,spriteW:integer,spriteH:integer}
    }=options;
    if (options.system){
        objectMerge(_system,options.system);
    }
    if (!_canvasID){
        console.error("flame: Не указан id-canvas");
        return null;
    }
    if (img && _canvasIDMidle){
        _system.context2dMiddle=document.getElementById(_canvasIDMidle).getContext("2d");
        _system.img=img;
    }
    _system.context2d=document.getElementById(_canvasID).getContext("2d");
    this.context2d = _system.context2d;
    Object.defineProperty(this,'_system',{
        get(){return _system;}
    });

    console.log(_system);
    return this;
};
_blA.flame.prototype.addFlame = function( angle,x, y ){
    const pieceCount = 15;
    for (let i=0;i<pieceCount;i++){
        this.pieces.push(new blowAnimate._piece({
            sys_param:this._system,
            v0:getRandomInRange(220,3620),
            angDegrees:getRandomInRange(angle - 5,angle + 5),
            weight:Math.random()/getRandomInRange(1,3),
            start_pos:{x:x, y:y},
            maxLive:10,
            piaceSize:2,
            reflect:false
        }));
    }
    if (!this.interval){
        this.interval = setInterval(() => {
            let tmp=[];
            this.context2d.clearRect(0,0,this.context2d.canvas.width, this.context2d.canvas.height);
            for (let i in this.pieces){
                if (this.pieces[i].step()){
                    tmp.push(this.pieces[i]);
                }
            }
            this.pieces=tmp;
            if (!this.pieces.length){
                console.log("flame end");
                clearInterval(this.interval);
                this.interval = 0;
            }
        }, 100);
    }
};

export const blowAnimate = _blA;
