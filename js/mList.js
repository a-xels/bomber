/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */
/* 
 Created on : 17 нояб. 2023 г., 22:59:57
 Author     : Щукин Александр.
 */



export const mList=function(){
    this.first = null;
    this.current = null;
};
mList.prototype.addEl = function(el){
    if (!this.current){
        this.current = {content:el, nextEl:null, prevEl:null};
    }else{
        while(this.current.nextEl !== null){
            this.current = this.current.nextEl;
        }
        this.current.nextEl = {content:el, nextEl:null, prevEl:this.current};
        this.current = this.current.nextEl;
    }
    if (!this.first){
        this.first = this.current;
    }
    return this;
};
mList.prototype.moveToStart = function(){
    if (this.first.content){
        this.current = this.first;
    }else{
        this.first = null;
    }
    return this.current !== null;
};
mList.prototype.moveNext = function(){
    if (this.current !== null && this.current.nextEl !== null){
        this.current = this.current.nextEl;
        return true;
    }else{
        this.current = null;
        return false;
    }
};
mList.prototype.removeCurrent = function(){
    if (this.current !== null){
        let setPrev = false;
        let setNext = false;
        if (this.current.prevEl !== null){
            this.current.prevEl.nextEl = this.current.nextEl;
            setPrev=true;
        }
        if (this.current.nextEl !== null){
            this.current.nextEl.prevEl = this.current.prevEl;
            setNext = true;
        }
//        delete this.current.content;
        if (!setPrev && !setNext){
            this.current = null;
            this.first = null;
        }else if (setPrev){
            this.current = this.current.prevEl;
        }else{
            this.current = this.current.nextEl;
        }
//        console.log(this.current);
        return true;
    }else{
        console.log('last', this.current);
        return false;
    }
};
mList.prototype.getCurrentEl = function(){
    if (this.current !== null){
        return this.current.content;
    }else{
        return null;
    }
};