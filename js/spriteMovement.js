/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */


import { Component } from './component.js';

export const SpriteMovement=function(options){
    let loadCounter=0;
    
    this._spritePos={
        X:0,
        Y:0
    };
    if (options.hasOwnProperty('spriteFileName')){
        if (typeof options.spriteFileName === "string"){
            loadCounter++;
            this.loadTotal++;
            this.img=[new Image()];
            this.img[0].src = options.spriteFileName;
            delete options.spriteFileName;
        }else{
            this.img=[];
            for (let i in options.spriteFileName){
                const _img=new Image();
                loadCounter++;
                this.loadTotal++;
                _img.src = options.spriteFileName[i];
                this.img.push( _img ); 
            }
            delete options.spriteFileName;
        }
        this.img.forEach( ( el ) => {
            el.addEventListener("load", (event) => {
                console.log((this.name?this.name:"SpriteMovement") + ": спрайт \""+el.src+"\" - загружен");
                loadCounter--;
                if ( typeof this.onLoadProgress === 'function' ){
                    this.onLoadProgress.call( this, this.loadTotal, loadCounter);
                }
                if (!loadCounter){
                    if ( typeof this.onLoad === 'function' ){
                        this.onLoad.call(this);
                    }else{
                        console.log((this.name?this.name:"SpriteMovement") + ": загрузки завершены.");
                    }
                }
            }); 
        } );
    }else{
        console.error((this.name?this.name:"SpriteMovement") + ": не передан \"spriteFileName\" - имя файла(ов) с спрайтом!");
        return this;        
    }
    if (options.hasOwnProperty('canvasId')){
        this.context2d = document.getElementById(options.canvasId).getContext("2d");
        delete options.canvasId;
    }else{
        console.error((this.name?this.name:"SpriteMovement") + ": не передан \"canvasId\" canvas");
        return this;       
    }
    Component.call( this,options,{
        isInfinityAnimation:false,
        isInfinityRotated:false,
        isRotateInversing:false,
        isClockwise:true,
        
        speedX:0,
        speedY:0,
        _speedX:0,
        _speedY:0,
        timerSpeed:0,
        frameSkip:0,
        _frameSkiper:0,
        rotateFrameSkip:0,
        _rotateFrameSkiper:0,
        rotateFrameSkiper:0,
        rotateStartAngel:0,
        rotateEndAngel:0,
        rotateStep:0,
        _rotateStep:0,
        _rotateAngel:0,
        outW:0,
        outH:0,
        changeSpeedX:0,
        changeSpeedY:0,
        _changeSpeedX:0,
        _changeSpeedY:0,
        minSpeedX:false,
        minSpeedY:false,
        maxSpeedX:false,
        maxSpeedY:false,
        currentSprite:0,
        onStepSkipper:0,
        _onStepSkipper:0,
        onLoad:null,
        onLoadProgress:null,
        onStep:null
        
    },true, [ 'speedX', 'outW', 'outH','timerSpeed' ] );
    this.loadTotal = 0;
    this.interval=0;
    this.maxH=this.context2d.canvas.height;
    this.maxW=this.context2d.canvas.width;

};
SpriteMovement.prototype=Object.create(Component.prototype);
Object.defineProperty(SpriteMovement.prototype, 'constructor', { 
    value: SpriteMovement, 
    enumerable: false, // false, чтобы данное свойство не появлялось в цикле for in
    writable: true 
});
SpriteMovement.prototype.doRotate = function(){
    this.context2d.setTransform(1, 0, 0, 1, this._spritePos.X, this._spritePos.Y);
    this.context2d.rotate(this._rotateAngel * Math.PI / 180);    
};
SpriteMovement.prototype.setNextRotate = function(){
    if (this.hasError || !this.rotateStep) return false;
    const aStep = Math.abs(this._rotateStep);
    if (this._rotateStep>0){
        this._rotateAngel += aStep;
        if (this._rotateAngel>this.rotateEndAngel){
            this._rotateAngel = this.rotateEndAngel;
            this._rotateStep = 0;
        }else{
            if (this._rotateAngel>360){
                this._rotateAngel = 360 - this._rotateAngel;
            }
        }
    }else{
        this._rotateAngel -= aStep;
        if (this._rotateAngel < this.rotateEndAngel){
            this._rotateAngel = this.rotateEndAngel;
            this._rotateStep = 0;            
        }else{
            if (this._rotateAngel<0){
                this._rotateAngel = 360 + this._rotateAngel;
            }
        }
    }
    this.doRotate();
    return true;
};
SpriteMovement.prototype.resetRotate = function(){
    this.context2d.setTransform(1,0,0,1,0,0);
};
SpriteMovement.prototype.reStart=function(x,y){
    if (this.hasError) return;
    if (this.interval){
        clearInterval(this.interval);
    }
    this.resetRotate();
    this._onStepSkipper = this.onStepSkipper;
    if (this.rotateStartAngel !== this.rotateEndAngel && Math.abs(this.rotateStep) > 0 ){
        this._rotateAngel = this.rotateStartAngel;
        this._rotateStep = this.rotateStep;
    }
    if (typeof x === 'number'){
        this._spritePos.X = x;
    }else if (this.hasOwnProperty('startX')){
        this._spritePos.X = this.startX;
    }else{
        this._spritePos.X = 0;
    }
    if (typeof y === 'number'){
        this._spritePos.Y = y;
    }else if (this.hasOwnProperty('startY')){
        this._spritePos.Y = this.startY;
    }else{
        this._spritePos.Y = 0;
    }
    this._speedX = this.speedX;
    this._speedY = this.speedY;
    this._changeSpeedX = this.changeSpeedX;
    this._changeSpeedY = this.changeSpeedY;
    
    
    this.interval=setInterval(()=>{
        if (!this.step()){
            clearInterval(this.interval);
            if (typeof this.endCallback === 'function'){
                this.endCallback.call(this);
            }
            this.interval=0;
        }        
    },this.timerSpeed);    
};
SpriteMovement.prototype.stop= function( skipCallBack ){
    if (typeof skipCallBack !=='boolean'){
        skipCallBack = false;
    }
    if (this.hasError) return;
    if (this.interval){
        clearInterval(this.interval);
        this.interval = 0;
    }
    if (this.rotateStep){
        this.doRotate();
        this.context2d.clearRect(
                -(this.outW/2 + 1),
                -(this.outH/2 + 1),
                this.outW+2,
                this.outH+2
        );
    }else{
        this.context2d.clearRect(
                this._spritePos.X-1,
                this._spritePos.Y-1,
                this.outW+2,
                this.outH+2
        );
    }
    if (!skipCallBack && typeof this.endCallback === 'function'){
        this.endCallback.call(this);
    }

};
SpriteMovement.prototype.isBusy = function(){
    return this.interval !== 0;
};
SpriteMovement.prototype.increasePosition=function(pos){
    if (pos.X>this.maxW){
       pos.X = -this.outW/2;
    }else{
        pos.X+=this._speedX;
        if (this._changeSpeedX){
            if (this._changeSpeedX){
                this._speedX += this._changeSpeedX;
                if (this.minSpeedX !== false && this._speedX < this.minSpeedX){
                    this._speedX = this.minSpeedX;
                    this._changeSpeedX=0;
                }else{
                    if (this.maxSpeedX !== false && this._speedX > this.maxSpeedX){
                        this._speedX = this.maxSpeedX;
                        this._changeSpeedX=0;
                    }
                }
            }
        }
    }
    if (pos.Y>this.maxH){
        return false;
    }else{
        pos.Y+=this._speedY;
        if (this._changeSpeedY){
            if (this._changeSpeedY){
                this._speedY += this._changeSpeedY;
                if (this.minSpeedY !== false && this._speedY < this.minSpeedY){
                    this._speedY = this.minSpeedY;
                    this._changeSpeedY=0;
                }else{
                    if (this.maxSpeedY !== false && this._speedY > this.maxSpeedY){
                        this._speedY = this.maxSpeedY;
                        this._changeSpeedY=0;
                    }
                }
            }
        }
        return true;
    }
};
SpriteMovement.prototype.step=function(){
    if (this.hasError) return;
    if (this.rotateStep){
        this.doRotate();
        this.context2d.clearRect(
                -(this.outW/2 + 1),
                -(this.outH/2 + 1),
                this.outW+2,
                this.outH+2
        );
    }else{
        this.context2d.clearRect(
                this._spritePos.X-1,
                this._spritePos.Y-1,
                this.outW+2,
                this.outH+2
        );
    }
    if (this.increasePosition(this._spritePos)){
        if (this.setNextRotate()){
            this.context2d.drawImage(
                this.img[this.currentSprite],
                -this.outW/2,
                -this.outH/2,
                this.outW,
                this.outH            
            );
        }else{
            this.context2d.drawImage(
                this.img[this.currentSprite],
                this._spritePos.X,
                this._spritePos.Y,
                this.outW,
                this.outH            
            );
        }
        this.resetRotate();
        if (typeof this.onStep === "function"){
            if (this._onStepSkipper <= 0){
                this._onStepSkipper = this.onStepSkipper;
                this.onStep.call(this, this._spritePos);
            }else{
                this._onStepSkipper--;
            }
        }
        return true;
    }else{
        this.resetRotate();
        return false;
    }
};
