/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */

export class Player{
    name;
    score;
    constructor(options){
        options = options || {};
        ({ name:this.name, score:this.score } = options);
    }
    toString(){
        return `${this.name} - score is ${this.score}`;
    }
    static createArray(dt){
        const rVal=[];
        for(let i = 0; i<dt.length; i++){
            rVal.push(new this(dt[i]));
        }
        return rVal;
    }
    static toSortASC(p1,p2){
        return p1.score - p2.score;
    }
    static toSortDSC(p1,p2){
        return p2.score - p1.score;
    }
}

