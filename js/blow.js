/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import { Component } from './component.js';
import { mList } from './mList.js';
export function getRandomInRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
function objectMerge(obj1,obj2){
    const keys=Object.keys(obj2);
    for (let i in keys){
        obj1[keys[i]]=obj2[keys[i]];
    }
}

export var blowAnimate=blowAnimate || {};
blowAnimate.busy=false;
blowAnimate._piece=function(options){
    Component.call(this,options,{
        maxLive:50,
        piaceSize:4,
        reflect:true
    },false);
    const _aRad=(this.angDegrees * Math.PI)/180;
    this.clearRect={
        _x:this.piaceSize + 2,
        _y:this.piaceSize + 2,
        w:( this.piaceSize + 2 ) * 2,
        h:( this.piaceSize + 2 ) * 2,
        x:function(X){return X - this._x;},
        y:function(Y){return Y - this._y;}
    };
    this.minLive=Math.floor(this.maxLive/5);
    this.minLive = this.minLive<2 || 2;
    this.ks=(this.sys_param.k*this.sys_param.airP)/(2*this.weight);
    this.v0x=this.v0*Math.cos(_aRad);
    this.v0y=this.v0*Math.sin(_aRad);
    this.t=0;
    this.x=0;
    this.y=0;
    this.cnt=getRandomInRange(this.minLive,this.maxLive);
    this.startCNT = this.cnt;
    return this;
};
blowAnimate._piece.prototype=Object.create(Component.prototype);
Object.defineProperty(blowAnimate._piece.prototype, 'constructor', { 
    value: blowAnimate._piece, 
    enumerable: false, // false, чтобы данное свойство не появлялось в цикле for in
    writable: true 
});
blowAnimate._piece.prototype.step=function(){
    let _x=this.start_pos.x+Math.round(this.x);
    let _y=this.start_pos.y-Math.round(this.y);
    //this.clearCircl(_x, _y);
    this.v0x=this.v0x-this.ks*Math.abs(this.v0x)*this.v0x*this.sys_param.tt;
    this.v0y=this.v0y-(this.sys_param.g + this.ks*Math.abs(this.v0y)*this.v0y)*this.sys_param.tt;
    this.x=this.x + this.v0x*this.sys_param.tt;
    this.y=this.y + this.v0y*this.sys_param.tt;
    _x=this.start_pos.x+Math.round(this.x);
    _y=this.start_pos.y-Math.round(this.y);
    if (_y>this.sys_param.context2d.canvas.height){
        this.v0y*=-1;
        this.y=this.y + this.v0y*this.sys_param.tt;
        return this.reflect;
    }
    if ((_x<5&&_x>-10) || (_x>this.sys_param.context2d.canvas.width-5 && _x<this.sys_param.context2d.canvas.width+10)){
        this.v0x*=-1;
        this.x=this.x + this.v0x*this.sys_param.tt;
        return this.reflect;
    }
    if (_y>0 && _y<this.sys_param.context2d.canvas.height && this.cnt && _x>0 && _x<this.sys_param.context2d.canvas.width){
        this.drawCircl(_x, _y);
        this.cnt--;
        return true;
    }else{
        return false;
    }
    };
blowAnimate._piece.prototype.getColor=function(){
    const colors=['248,232,0','248,136,0','225,255,255','152,16,0','248,232,0','225,255,255','211,13,0','216,32,0','248,232,0','225,255,255','96,1,0','225,255,255'];
    return 'rgba('+colors[getRandomInRange(0,colors.length-1)]+','+(this.cnt/this.maxLive)+')';
};
blowAnimate._piece.prototype.clearCircl=function(x,y){
    this.sys_param.context2d.beginPath();
    this.sys_param.context2d.clearRect(
        this.clearRect.x(x),
        this.clearRect.y(y),
        this.clearRect.w,
        this.clearRect.h
    );
    this.sys_param.context2d.closePath();
};
blowAnimate._piece.prototype.drawCircl=function(x,y){
    this.sys_param.context2d.beginPath();
    this.sys_param.context2d.fillStyle=this.getColor();
    this.sys_param.context2d.arc(x, y, this.piaceSize, (getRandomInRange(0,180) * Math.PI)/180, (getRandomInRange(181,359) * Math.PI)/180, true);
    this.sys_param.context2d.fill();
    this.sys_param.context2d.closePath();
};

blowAnimate._blows=function(options){
    options=options || {};
    const _system = {
            airP : .029,       //Плотность воздуха
            k    : .024,       //Геометрический коофициент сопр
            tt   : .0065,       //Шаг итерации
            g    : 9.8         //Притяжение
    };
    this.blowsProcesing=[];
    this.interval=0;
    const {
        id:_canvasID =  "",             //ID - элемента canvas
        idMidle:_canvasIDMidle =  "",   //ID - элемента canvas по середине
        img                             //Изображение обект (content:img,spriteW:integer,spriteH:integer}
    }=options;
    if (options.system){
        objectMerge(_system,options.system);
    }
    if (!_canvasID){
        console.error("Не указан id-canvas");
        return null;
    }
    if (img && _canvasIDMidle){
        _system.context2dMiddle=document.getElementById(_canvasIDMidle).getContext("2d");
        _system.img=img;
    }
    _system.context2d=document.getElementById(_canvasID).getContext("2d");
    Object.defineProperty(this,'_system',{
        get(){return _system;}
    });

    console.log(_system);
    return this;
};
blowAnimate._blows.prototype.addBlow=function(x,y){
    let tmp;
    if (blowAnimate.busy){
        console.error("Невозможно добавить занят.");
        return;
    }
    blowAnimate.busy=true;
    this.blowsProcesing.push(new blowAnimate._blow({
        system:this._system,
        x:x,
        y:y
    }));
    if (!this.interval){
        this.interval=setInterval(()=>{
            tmp=[];
            for (let i=0;i<this.blowsProcesing.length;i++){
                if (this.blowsProcesing[i].step()){
                    tmp.push(this.blowsProcesing[i]);
                }
            }
            this.blowsProcesing=tmp;
            if (!this.blowsProcesing.length){
                clearInterval(this.interval);
                this.interval=0;
                console.log("all blows end");        
            }
        },30);
    }
    blowAnimate.busy=false;
};
blowAnimate._blows.prototype.clearSprite=function( worcRect ){
    this._system.context2dMiddle.beginPath();
    this._system.context2dMiddle.clearRect(
        worcRect.x,
        worcRect.y,
        worcRect.w,
        worcRect.h
    );
    this._system.context2dMiddle.closePath();   
};
blowAnimate._blows.prototype.addSpriteBlow=function(x,y){
    if (this._system.context2dMiddle && this._system.img){
        if (!this._system.img.mult) this._system.img.mult=1;
        if (!this._system.img.yDivider) this._system.img.yDivider=2;
        const cCnt=Math.floor(this._system.img.content.width/this._system.img.spriteW);
        const rCnt=Math.floor(this._system.img.content.height/this._system.img.spriteH);
        let halfStep=Math.floor((cCnt*rCnt)/1.5);
        const time=Math.floor(630/(cCnt*rCnt));
        let c=0;
        let r=0;
        let interval2=0;
        const worcRect={
            x:x-(this._system.img.spriteW*this._system.img.mult)/2,
            y:y-(this._system.img.spriteH*this._system.img.mult)/this._system.img.yDivider,
            w:(this._system.img.spriteW*this._system.img.mult),
            h:(this._system.img.spriteH*this._system.img.mult)
        };
        interval2=setInterval(()=>{
            halfStep--;
            if (halfStep===0){
                this.addBlow(x,y-10);
            }
            this.clearSprite(worcRect);
//            console.log(c,r);
            this._system.context2dMiddle.beginPath();
            this._system.context2dMiddle.drawImage(this._system.img.content,
                c++*this._system.img.spriteW,
                r*this._system.img.spriteH,
                this._system.img.spriteW,
                this._system.img.spriteH,
                worcRect.x,
                worcRect.y,
                worcRect.w,
                worcRect.h
            );
            this._system.context2dMiddle.closePath();
            if (c>=cCnt){
                c=0;
                if (++r>=rCnt){
                    r=0;
                    clearInterval(interval2);
                    this.clearSprite(worcRect);
                }
            }
        },time);
    }
};
blowAnimate._blow=function(options){
    options=options || {};
    const {
        system:_system,
        x:x,                        //старт X
        y:y                        //старт Y
    } = options;
    const pieceCount =1800;
    this.pieces=[];
//    this.pieces = new mList();
    console.log("blow start"); 
    for (let i=0;i<pieceCount;i++){
        this.pieces.push(new blowAnimate._piece({
            sys_param:_system,
            v0:getRandomInRange(520,6620),
            angDegrees:getRandomInRange(-10,190),
            weight:Math.random()/getRandomInRange(1,6),
            start_pos:{x:x, y:y},
            maxLive:50
        }));
    }
    this.context2d = _system.context2d;
//    console.log(this);
    return this;
};
blowAnimate._blow.prototype.step=function(){
        let tmp=[];
        this.context2d.clearRect(0,0,this.context2d.canvas.width, this.context2d.canvas.height);
        for (let i=0;i<this.pieces.length;i++){
            if (this.pieces[i].step()){
                tmp.push(this.pieces[i]);
            }
        }
        this.pieces=tmp;
        if (!this.pieces.length){
            console.log("blow end");
            return false;
        }else{
            return true;
        }
//    if (this.pieces.moveToStart()){
//        do{
//            if (!this.pieces.getCurrentEl().step()){
//                this.pieces.removeCurrent();
//            }
//        }while (this.pieces.moveNext());
//        return true;
//    }else{
//            console.log("blow end");
//            return false;
//    }
};