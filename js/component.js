/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


export const Component=function(options, defaultValue, checkExist, required){
    let _hasError = false;
    options=typeof options === 'object'?options:{};
    checkExist = typeof checkExist === 'boolean'?checkExist:true;
    defaultValue = typeof defaultValue === 'object'?defaultValue:{};
    const keysOptions = Object.keys( options );
    const keysDefaultValue = Object.keys( defaultValue );
    Object.defineProperty( this, 'hasError', {
        get:function(){ return _hasError;},
        set:function(){_hasError=true;}
    } );
    if ( required instanceof Array ){
        for ( let i = 0;i<required.length;i++ ){
            if ( !options.hasOwnProperty( required[i] ) && !this.hasOwnProperty( required[i] ) ){
                console.error( `Component: необходимо указать свойство "${required[i]}"` );
                _hasError = true;
            }
        }
        if ( _hasError ){
            return this;
        }
    }
    for ( let i in keysDefaultValue ){
        if ( !this.hasOwnProperty( keysDefaultValue[i] ) ){
            this[keysDefaultValue[i]] = defaultValue[keysDefaultValue[i]];
        }
    }
    for ( let i in keysOptions ){
        if ( this.hasOwnProperty( keysOptions[i] ) ){
            this[keysOptions[i]] = options[keysOptions[i]];
        }else if ( !checkExist ){
            Object.defineProperty( this,keysOptions[i],{
                get:function(){return options[keysOptions[i]];}
            } );
        }else{
            console.warn( `Component: установка неизвестного свойства "${keysOptions[i]}"` );
        }
    }
    return this;
};
export const Test=function(options){
    this.prop1=null;
    let prop2=null;
    Component.call(this,options,false);
};

Test.prototype=Object.create(Component.prototype);
Object.defineProperty(Test.prototype, 'constructor', { 
    value: Test, 
    enumerable: false, // false, чтобы данное свойство не появлялось в цикле for in
    writable: true 
});

