/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */
/* 
 Created on : 29 нояб. 2023 г., 22:39:41
 Author     : Щукин Александр.
 */
import { Page } from './Page.js';
import { Player } from './Player.js';
import { VerticalDiv, HorizonDiv, TextItem, Border, Padding } from './Panel.js';
export class StartPage extends Page{
    url;
    players=[];
    constructor(options){
        super(options);
        ({ 
            scoreListUrl:this.url
        } = options);
        if (typeof this.url !== 'string'){
            this.error = true;
            console.error("StartPage: не передан \"scoreListUrl\"");
            return this;       
        }
        this.context2d.font = "44px serif";
    }
    run(){
        if (this.error) return;
        this.clearPort();
        const xhr = new XMLHttpRequest();
        xhr.open("POST", this.url);
        xhr.setRequestHeader("Content-Type", "application/json; charset=UTF-8");
        xhr.onload = () => {
            if (xhr.readyState == 4 && (xhr.status == 201 || xhr.status == 200)) {
                const answObj = xhr.responseText?JSON.parse(xhr.responseText):{};
                console.log(answObj);
                this.players = Player.createArray(answObj.answer).sort(Player.toSortDSC);
                console.log(this.players);
                const div= new VerticalDiv({
                    context2d:this.context2d,
                    color:"#F2A",
                    backgroundColor:"#fff",//"#33afaf",
                    border:new Border({thickness:4,color:'#FF5',radius:5}),
                    width:103,
                    padding:new Padding(10,20),
                });
                div.addItem(new TextItem({
                    text:"Test",
                    context2d:div.content2d,
                    color:"#F2A",
                    border:new Border({thickness:1,color:'red'}),
                    //padding:new Padding(15)
                }));
                div.addItem(new TextItem({
                    text:"string",
                    context2d:div.content2d,
                    color:"#F2A",
                    border:new Border({thickness:1,color:'black'}),
                    //padding:new Padding(15)
                }));
                div.addItem(new TextItem({
                    text:"!",
                    context2d:div.content2d,
                    color:"#F2A",
                    border:new Border({thickness:1,color:'green'}),
                    //padding:new Padding(15)
                }));
                div.render(false,200);
            } else {
                this.error = true;
                console.log(xhr, `Error: ${xhr.status}`);
            }
        };
        xhr.send(JSON.stringify({
            action:'get-list'
        }));
        console.log("StartPage: выполняем запрос");
    }
}

