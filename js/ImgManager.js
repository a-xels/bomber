/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */
/* 
 Created on : 29 нояб. 2023 г., 19:04:39
 Author     : Щукин Александр.
 */
export class ImgManager{
    _imgList={};
    constructor(options){
        options = options || {};
    }
    _hashString(str){
        let hash = 0,
          i, chr;
        if (str.length === 0) return hash;
        for (i = 0; i < str.length; i++) {
          chr = str.charCodeAt(i);
          hash = ((hash << 5) - hash) + chr;
          hash |= 0; // Convert to 32bit integer
        }
        return hash;
    }
    getImg(name, callBack){
        const key = this._hashString(name);
        let rVal;
        if (!Object.hasOwnProperty(this._imgList, key)){
            rVal=new Image;
            rVal.src = name;
            console.log('disk');
            rVal.onload = () => {
                
                if ( typeof callBack === 'function' ){
                    callBack.call(rVal, false);
                }
            };
            this._imgList[key] = {img : rVal, name:name};
        }else{
            rVal = this._imgList[key].img;
            console.log('cache');
            if ( typeof callBack === 'function' ){
                callBack.call(rVal, true);
            }            
        }
        return rVal;
    }
}

