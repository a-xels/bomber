/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */


import { SpriteMovement } from './spriteMovement.js';

export const Bomb=function(options){
    this.name="Bomb";
    this.speedX = 5;
    this.speedY = 2;
    this.timerSpeed=30;
    this.outW = 29,5;
    this.outH = 9;
    this.changeSpeedX = -.045;
    this.changeSpeedY = .1;
    this.minSpeedX=0;
    this.rotateStartAngel = 0;
    this.rotateEndAngel = 90;
    this.rotateStep = 1;
    options=typeof options === 'object'?options:{};
    this.endCallback=function(){console.log("Bomb - is end.");};
    SpriteMovement.call(this,options);
};
Bomb.prototype=Object.create(SpriteMovement.prototype);
Object.defineProperty(Bomb.prototype, 'constructor', { 
    value: Bomb, 
    enumerable: false, // false, чтобы данное свойство не появлялось в цикле for in
    writable: true 
});
