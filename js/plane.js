/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import { SpriteMovement } from './spriteMovement.js';
export const Plane=function(options){
    this.name="Plane";
    this.speedX=3;
    this.speedY=25;
    this.timerSpeed=15;
    this.outW=117;
    this.outH=36;
    this.startX = -117;
    this.endCallback=function(){console.log("Plane - is end.");};
    SpriteMovement.call(this,options);
    return this;
};
Plane.prototype=Object.create(SpriteMovement.prototype);
Object.defineProperty(Plane.prototype, 'constructor', { 
    value: Plane, 
    enumerable: false, // false, чтобы данное свойство не появлялось в цикле for in
    writable: true 
});
Plane.prototype.increasePosition=function(pos){
    if (pos.X>this.maxW){
        pos.Y+=this.speedY;
        if (pos.Y+this.outH>this.maxH){
            return false;
        }else{
            pos.X=-this.outW;
        }
    }else{
        pos.X+=this.speedX;
    }
    return true;
};

