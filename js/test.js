/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */

const blows=new blowAnimate._blows({id:'canvas-display'});
blows.addBlow(100,100);
console.log(blows);
function getRandomInRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
const start={
    x:800,
    y:700
};
function system(tt=0.005,airP=.029,k=.024){
    return {
        p : airP, //Плотность воздуха
        k : k,    //Геометрический коофициент сопр
        tt:tt,     //Шаг итерации
        g:9.8    //Притяжение
    };
}
function fr(v0, ang, m, start_pos, sys_param){
    //v0 - нач.скор
    //ang - угол в градусах
    //m - масса осколка в кг.
    //start_pos - начальные координаты
    //sys_param - параметры среды
    const _aRad=(ang * Math.PI)/180;
    const _div=document.createElement('div');
    const colors=['#ffcc66','#ff9966','#fc1117','#fc8529','#fc3f06'];
    _div.className="bullet";
    document.body.appendChild(_div);
    _div.style['background-color']=colors[getRandomInRange(0,4)];
    return{
        ks:(sys_param.k*sys_param.p)/(2*m),
        v0x:v0*Math.cos(_aRad),
        v0y:v0*Math.sin(_aRad),
        t:0,
        x:0,
        y:0,
        cnt:getRandomInRange(2,80),
        div:_div,
        step:function(){
            this.v0x=this.v0x-this.ks*Math.abs(this.v0x)*this.v0x*sys_param.tt;
            this.v0y=this.v0y-(sys_param.g + this.ks*Math.abs(this.v0y)*this.v0y)*sys_param.tt;
            this.x=this.x + this.v0x*sys_param.tt;
            this.y=this.y + this.v0y*sys_param.tt;
            const _x=start_pos.x+Math.round(this.x);
            const _y=start_pos.y-Math.round(this.y);
            if (_y>0 && this.cnt && _x>0){
                this.div.style.top=_y+'px';
                this.div.style.left=_x+'px';
                //if (this.cnt<201){
                this.div.style.opacity=this.cnt/80;
//                this.div.style.filter="saturate("+(this.cnt/80*100)+"%)";
                this.div.style['background-color']=colors[getRandomInRange(0,4)];
                this.cnt--;
                return true;
            }else{
                this.div.remove();
                return false;
            }
        }
    };
}

const sys=new system();
let elements=[];
let tmp=[];
const elCnt=1550;
const button = document.querySelector("body");

button.addEventListener("click", (event) => {
//  button.textContent = `Click count: ${event.detail}`;
    console.log(event);
    const st={
        x:event.clientX,
        y:event.clientY
    };
    for(let i=0;i<elCnt;i++){
        elements.push(new fr(getRandomInRange(620,1500),getRandomInRange(getRandomInRange(-35,89),getRandomInRange(91,215)),Math.random()/getRandomInRange(1,26),st,sys));
        
    }
    console.log(elements);
    console.log("start");
    const interval=setInterval(()=>{
        let cont=false;
        tmp=[];
        for (let i=0;i<elements.length;i++){
            if (elements[i].step()){
                tmp.push(elements[i]);
            }
            //cont=elements[i].step() || cont;
        }
        element=tmp;
        if (!element.length){
            clearInterval(interval);
            console.log("end");        
        }else{
            //console.log("cnt: ",element.length);
        }
    //    if (!cont){
    //        clearInterval(interval);
    //        console.log("end");
    //    }
    },45);

});
//for(let i=0;i<elCnt;i++){
//    elements.push(new fr(getRandomInRange(10,1000),getRandomInRange(-10,190),getRandomInRange(1,3),start,sys));
//}

//const aRad=(al * Math.PI)/180;
//const v0x=v0*Math.cos(aRad);
//const v0y=v0*Math.sin(aRad);
//let t=0;
//let y=0;
//const div=document.createElement('div');
//div.className="bullet";
//document.body.appendChild(div);
//let interval=setInterval(()=>{
//    if (y>=0){
//        const x=v0x*t;
//        y=v0y*t-(9.8*(t*t))/2;
//        div.style.bottom=y+'px';
//        div.style.left=x+'px';
//        console.log("t=",t,' x=',x,' y=',y);
//        t+=.1;
//    }else{
//        div.remove();
//        clearInterval(interval);
//    }
//},50);
