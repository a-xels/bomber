/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


export const citySprite = [
    {
        mainFiles:[
            'img/home1/fStart.png',
            'img/home1/f0.png',
            'img/home1/f1.png',
            'img/home1/fend.png'
        ],
        mainSpriteXShift:[0, 108, 108, 108],
        outW:130
    },
    {
        mainFiles:[
            'img/home3/fStart.png',
            'img/home3/f0.png',
            'img/home3/f1.png',
            'img/home3/f2.png',
            'img/home3/f3.png',
            'img/home3/f4.png',
            'img/home3/f5.png',
            'img/home3/f6.png',
            'img/home3/f7.png',
            'img/home3/f8.png',
            'img/home3/f9.png',
            'img/home3/f10.png',
            'img/home3/f11.png',
            'img/home3/f12.png',
            'img/home3/f13.png',
            'img/home3/f14.png',
            'img/home3/f15.png',
            'img/home3/f0.png'
        ],
        outW:70
    },
    {
        mainFiles:[
            'img/home2/fStart.png',
            'img/home2/f0.png',
            'img/home2/f1.png',
            'img/home2/f2.png',
            'img/home2/f3.png',
            'img/home2/fend.png'
        ],
        outW:70
    },
    {
        mainFiles:[
            'img/home4/fStart.png',
            'img/home4/f0.png',
            'img/home4/f1.png',
            'img/home4/f2.png',
            'img/home4/f3.png',
            'img/home4/f4.png',
            'img/home4/fend.png'
        ],
        outW:75
    }
];
