/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */
/* 
 Created on : 23 нояб. 2023 г., 22:33:24
 Author     : Щукин Александр.
 */
import { Component } from './component.js';
import { getRandomInRange }from './blow.js';
export const Bilding = function(options){
    let fNm = [];
    options = options || {};
    if (options.hasOwnProperty('canvasId')){
        this.context2d = document.getElementById(options.canvasId).getContext("2d");
        delete options.canvasId;
    }else{
        console.error((this.name?this.name:"Bilding") + ": не передан \"canvasId\" canvas");
        return this;       
    }
    if (options.hasOwnProperty('mainFiles')){
        fNm = options.mainFiles;
        delete options.mainFiles;
    }else{
        console.error((this.name?this.name:"Bilding") + ": не передан \"mainFiles\" canvas");
        return this;       
    }
    Component.call( this, options, {
        startX:0,
        height:3,
        onLoad:null,
        onLoadProgress:null,
        outW:0,
        mainSpriteXShift:[],
        iManager:null
    }, true, [ 'startX', 'iManager' ] );
    this.startY = this.context2d.canvas.height;
    this._loading = 0;
    this.loadTotal = 0;
    this.loadMainSprites( fNm );
    this._heightPX = 0;
    this._widthPX = 0;
    this._ratio = 1;
    this.areas=[];
    return this;
};
Bilding.prototype=Object.create(Component.prototype);
Object.defineProperty(Bilding.prototype, 'constructor', { 
    value: Bilding, 
    enumerable: false, // false, чтобы данное свойство не появлялось в цикле for in
    writable: true 
});
Bilding.prototype.loadMainSprites = function(fNames){
    this.mainSpritest = [];
    fNames.forEach((element) => {
        this._loading++;
        this.loadTotal++;
        const img = this.iManager.getImg(element, () => {
            this._loading--;
            if ( typeof this.onLoadProgress === 'function' ){
                this.onLoadProgress.call( this, this.loadTotal, this._loading);
            }else{
                console.log("Bilding: загружен.");
            }
            if (!this._loading){
                if (typeof this.onLoad === 'function'){
                    this.onLoad.call(this);
                }else{
                    console.log("Bilding: загрузки завершены.");
                }
            }
        });
        this.mainSpritest.push(img);
    } );
};
Bilding.prototype.renderFloor = function(num,_y){
    const outH = this.mainSpritest[ num ].height * this._ratio;
    const outW = this.mainSpritest[ num ].width  * this._ratio;
    const X = this.startX + (num < this.mainSpriteXShift.length ? ( this.mainSpriteXShift[num] * this._ratio ) : 0);
    const Y = _y - outH;
    this.context2d.drawImage(
        this.mainSpritest[num],
        X, Y,
        outW,
        outH            
    );
    return { x:X, y:Y, w:outW, h:outH, mainSpriteNum: num};
};
Bilding.prototype.renderBulding = function(){
    let Y = this.startY;
    const maxRandom = this.mainSpritest.length - 2;
    this.areas = [];
    if (!this.outW){
        this.outW = this.mainSpritest[0].width;
    }
    this._ratio = this.outW / this.mainSpritest[0].width;
    this.areas.push( this.renderFloor( 0, Y ) );
    Y -= this.areas[this.areas.length - 1].h;
    for (let i=0; i<this.height; i++){
        this.areas.push( this.renderFloor( getRandomInRange( 1, maxRandom ), Y ) );
        Y -= this.areas[this.areas.length - 1].h;        
    }
    this.areas.push( this.renderFloor( this.mainSpritest.length - 1, Y ) );
};
Bilding.prototype.removeAreaAndStartBlowing = function(areaNumber){
    
};