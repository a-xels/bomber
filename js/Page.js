/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */
/* 
 Created on : 29 нояб. 2023 г., 22:47:31
 Author     : Щукин Александр.
 */



export class Page{
    onAction;
    context2d;
    error=false;
    constructor(options){
        options = options || {};
        ({ onAction:this.onAction } = options);
        let { 
            canvasId:_canvasId
        } = options;
        if (typeof _canvasId === 'string'){
            this.context2d = document.getElementById(options.canvasId).getContext("2d");
        }else{
            this.error = true;
            console.error("Page: не передан \"canvasId\" canvas");
            return this;       
        }
        //console.log(this);
    }
    clearPort(){
        if (this.error) return;
        this.context2d.clearRect( 0, 0, this.context2d.canvas.width, this.context2d.canvas.height );
    }
    run(){console.log("Page:run()");}
}