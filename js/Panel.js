/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */
export class Padding{
    top;right;bottom;left;
    constructor(options,r,b,l){
        if (options instanceof Object){
            ({
                top : this.top = 0,
                right : this.right = this.top,
                bottom : this.bottom = this.top,
                left : this.left = this.right
            } = options);
        }else{
            this.top = typeof(options) === 'number' ? options :0;
            this.right = typeof(r) === 'number' ? r : this.top;
            this.bottom = typeof(b) === 'number' ? b : this.top;
            this.left = typeof(l) === 'number' ? l : this.right;
        }
    }
}
export class Atom{
    error;color;backgroundColor;
    constructor(options){
        options = options || {};
        ({ 
            color : this.color = "#000",
            backgroundColor : this.backgroundColor = ''
        } = options);
        this.error = false;
    }
}
export class Border extends Atom{
    thickness;radius;
    constructor(options){
        super(options);
        ({
            thickness : this.thickness = 1,
            radius : this.radius = 0
        }=options);
    }
    render(context2d){
        const path1 = new Path2D();
        let _x = 0;
        let _y = 0;
        let w = context2d.canvas.width;
        let h = context2d.canvas.height;
        let i = this.thickness;
        let radRadius = this.radius ? ( this.radius ) : 0;
        do{
            if (radRadius){
                if (w < 2 * radRadius) radRadius = w / 2;
                if (h < 2 * radRadius) radRadius = h / 2;
                path1.moveTo(_x+radRadius, _y);
                path1.arcTo(_x + w, _y, _x + w, _y + h, radRadius);
                path1.arcTo(_x + w, _y+h, _x, _y + h, radRadius);
                path1.arcTo(_x, _y + h, _x, _y, radRadius);
                path1.arcTo(_x, _y, _x + w, _y, radRadius);
                _x+=.5;_y+=.5;h-=1;w-=1;i-=.5;
            }else{
                path1.rect(_x++,_y++,w,h);
                h-=2;w-=2;i--;
            }
        }while(i>0);
        path1.closePath();
        context2d.strokeStyle = this.color;
        context2d.stroke(path1);
   }
}
export class Item extends Atom{
    error;width;height;context2d;padding;border;
    content2d;isAutoWidth;isAutoHeight;
    constructor(options){
        super(options);
        options = options || {};
        ({ 
            width : this.width,
            height : this.height,
            context2d : this.context2d,
            padding : this.padding,
            border : this.border
        } = options);
        this.isAutoWidth = typeof(options.width) === 'undefined';
        this.isAutoHeight = typeof(options.height) === 'undefined';
        if (!this.context2d){
            this.error = true;
            console.error("Item: context2d - not set.");
            return;
        }
        if (!this.width){
            this.width = 2;
            console.warn("Item: width - not set.");
        }
        if (!this.height){
            this.height = 2;
            console.warn("Item: height - not set.");
        }
        if (! (this.padding instanceof Padding)){
            this.padding = new Padding( 5 );
        }
        this._canvas = document.createElement('canvas');
        this.content2d = this._canvas.getContext("2d");
        this._recalculateSize();
    }
    _recalculateSize(){
        let _w = this.width;
        let _h = this.height;
        if (this.border instanceof Border){
            _w+=this.border.thickness*2;
            _h+=this.border.thickness*2;
        }
        this.content2d.canvas.width = _w + this.padding.left + this.padding.right;
        this.content2d.canvas.height = _h + this.padding.top + this.padding.bottom; 
        this.content2d.font = this.context2d.font;
    }
    _renderBorder(){
        if (this.border instanceof Border){
            this.border.render( this.content2d );
        }        
    }
    _renderBackground(){
        if (this.backgroundColor){
            const hasB = this.border instanceof Border;
            const x=0 + (hasB?this.border.thickness:0);
            const y=0 + (hasB?this.border.thickness:0);
            const w=this.content2d.canvas.width - (hasB?this.border.thickness:0);
            const h=this.content2d.canvas.height - (hasB?this.border.thickness:0);
            this.content2d.beginPath();
            this.content2d.fillStyle = this.backgroundColor;
            this.content2d.fillRect(x,y,w,h);
            this.content2d.closePath();
        }        
    }
    checkX(x){
        return typeof(x) === 'number' ? x:Math.floor(this.context2d.canvas.width/2 - this.width/2);
    }
    checkY(y){
        return typeof(y) === 'number' ? y:Math.floor(this.context2d.canvas.height/2 - this.height/2);
    }
    outWidth(w){
        if (typeof(w) === 'number'){
            this.content2d.canvas.width = w;
            this.content2d.font = this.context2d.font;
        }
        return this.content2d.canvas.width;
    }
    outHeight(h){
        if (typeof(h) === 'number'){
            this.content2d.canvas.height = h;
            this.content2d.font = this.context2d.font;
        }
        return this.content2d.canvas.height;
    }
    _run(){
        if (this.error) return;
        this._renderBackground();
    }
    render(x,y, _w, _h){
        const oW = this.outWidth();
        const oH = this.outHeight();
        const w = typeof(_w) === 'number' ? _w : oW;
        const h = typeof(_h) === 'number' ? _h: oH;
        this._run();
        this._renderBorder();
        if (w!==oW || h!==oH){
            this.context2d.drawImage(
                    this.content2d.canvas,
                    0,0,
                    w,h,
                    this.checkX(x), 
                    this.checkY(y),
                    w,h
            );
        }else{
            this.context2d.drawImage( this.content2d.canvas, this.checkX(x), this.checkY(y));
        }
    }
}
export class TextItem extends Item{
    text;tWidth;tHeight;textAlign;
    constructor(options){
        super( options );
        ({ 
//            text: this._text = " ",
            textAlign: this.textAlign = 'left'
        } = options);
        this.setText(options.text);
        console.log(this);
    }
    setText(val){
        this._text = typeof(val)==='string' ? val :' ';
        let mText;
        mText = this.context2d.measureText(this._text);
        if (!this.isAutoWidth){
            let tmpText = this._text;
            let pos = tmpText.length - 1;
            while(tmpText.length > 1 && mText.width  > this.content2d.canvas.width - this.padding.left - this.padding.right - (this.border?this.border.thickness*2:0)){
                pos--;
                tmpText = this._text.slice(0,pos) + '...';
                mText = this.context2d.measureText(tmpText);
            }
            this._text = tmpText;
        }
        this.tWidth = mText.width;
        this.tHeight = mText.actualBoundingBoxAscent + mText.actualBoundingBoxDescent;
        if (this.isAutoWidth){
            this.width = this.tWidth;
        }
        if (this.isAutoHeight){
            this.height = this.tHeight;
        }
        
        this._recalculateSize();        
    }
    _computeY(){
        return Math.round(this.padding.top+(this.content2d.canvas.height-this.padding.top-this.padding.bottom)/2);
    }
    _run(){
        if (this.error) return;
        super._run();
        const mW = this.content2d.canvas.width - this.padding.left - this.padding.right - (this.border?this.border.thickness*2:0);
        this.content2d.beginPath();
        this.content2d.fillStyle=this.color;
        if (this.tWidth < mW){
            let posX;
            switch(this.textAlign){
                case 'center':
                    posX = Math.floor(this.content2d.canvas.width/2 - this.tWidth/2);
                    break;
                case 'right':
                    posX = this.content2d.canvas.width - this.padding.right - this.tWidth;
                    break;
                default:
                    posX = this.padding.left;
                    break;
            }
            this.content2d.textBaseline = "middle";
            this.content2d.fillText(this._text, posX,this._computeY());
        }else{
            this.content2d.textBaseline = "middle";
            this.content2d.fillText(this._text, this.padding.left,this._computeY());
        }
        this.content2d.closePath();
    }
}
export class HorizonDiv extends Item{
    children = [];
    addItem(item){
        let doRecalc = false;
        const cmpW = this.children.reduce(function(accumulator,el){
            return accumulator + el.outWidth();
        },0);
        this.children.push(item);
        if (cmpW + item.outWidth()>this.width && this.isAutoWidth){
            this.width = cmpW + item.outWidth();
            doRecalc = true;
        }
        if (item.outHeight()>this.height && this.isAutoHeight){
            this.height = item.outHeight();
            this.children.forEach((el)=>{
                el.outHeight(this.height);
            });
            doRecalc = true;
        }else{
            item.outHeight(this.height);
        }
        if(doRecalc){
            this._recalculateSize();
        }
    }
    _run(){
        super._run();
        const _y = this.padding.top + (this.border?this.border.thickness:0);
        let _x = this.padding.left + (this.border?this.border.thickness:0);
        const mW = this.content2d.canvas.width - this.padding.right - (this.border?this.border.thickness:0);
        for (let i = 0; i < this.children.length && _x  <= mW + 1; i++){
            if(_x + this.children[i].outWidth() <= mW + 1){
                this.children[i].render(_x, _y);
            }else{
                this.children[i].render(_x, _y, this.children[i].outWidth() - ( _x + this.children[i].outWidth() - mW ) );
            }
            _x += this.children[i].outWidth();
        }
    }
}


export class VerticalDiv extends Item{
    children = [];
    addItem(item){
        let doRecalc = false;
        const cmpH = this.children.reduce(function(accumulator,el){
            return accumulator + el.outHeight();
        },0);
        this.children.push(item);
        if (cmpH + item.outHeight()>this.height && this.isAutoHeight){
            this.height = cmpH + item.outHeight();
            doRecalc = true;
        }
        if (item.outWidth()>this.width && this.isAutoWidth){
            this.width = item.outWidth();
            this.children.forEach((el)=>{
                el.outWidth(this.width);
            });
            doRecalc = true;
        }else{
            item.outWidth(this.width);
        }
        if(doRecalc){
            this._recalculateSize();
        }
    }
    _run(){
        super._run();
        let _y = this.padding.top + (this.border?this.border.thickness:0);
        const _x = this.padding.left + (this.border?this.border.thickness:0);
        const mH = this.content2d.canvas.height - this.padding.bottom - (this.border?this.border.thickness:0);
        for (let i = 0; i < this.children.length && _y  <= mH + 1; i++){
            if(_y + this.children[i].outHeight() <= mH + 1){
                this.children[i].render(_x, _y);
            }else{
                this.children[i].render(_x, _y, this.children[i].outWidth(), this.children[i].outHeight() - ( _x + this.children[i].outHeight() - mH ) );
            }
            _y += this.children[i].outHeight();
        }
    }
}