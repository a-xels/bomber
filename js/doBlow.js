/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import { blowAnimate }from './flame.js';
import { Plane }from './plane.js';
import { Bomb } from './bomb.js';
import { Bilding } from './bilding.js';
import { City } from './city.js';
import { citySprite } from './citysprite.js';
import { ImgManager } from './ImgManager.js';

const iManager = new ImgManager();
console.log(iManager._hashString('test1'));

let loadCounter = 0;
let totalLoad = 0;
function loadProgressImg(total, current){
    const el = document.getElementById("load-progress");
//    el.setAttribute('max',total);
//    el.setAttribute("value",total - current);
//    el.textContent = Math.round( (total - current)/ total * 100) + "%";
    loadCounter--;
    el.setAttribute('max',totalLoad);
    el.setAttribute("value",totalLoad - loadCounter);
    el.textContent = Math.round( (totalLoad - loadCounter)/ totalLoad * 100) + "%";
}
const img=new Image();
img.src="img/blow2.png";
const blows=new blowAnimate._blows({
    id:'canvas-display-blow',
    idMidle:'canvas-display-midle',
    img:{
        content:img,
        spriteW:256,
        spriteH:256,
        mult:.8,
        yDivider:1.2
    }
});
const city = new City({
    canvasId:'canvas-display-back',
    fileNameList:citySprite,
    onImgLoadProgress:loadProgressImg,
    iManager:iManager,
    onLoad:() => {
        loadCounter--;
        if (!loadCounter){
            start();
        }
    }
});
totalLoad = city.prepareRender(20, 3);
loadCounter = totalLoad + 2;
const flame=new blowAnimate.flame({
    id:'canvas-display-flame',
    idMidle:'canvas-display-midle'
}); 
const plane1 = new Plane({
    canvasId:'canvas-display-top',
    spriteFileName:['img/plane2.png', 'img/plane.png'],
    onStepSkipper:2,
    onStep:function(pos){
        flame.addFlame(180,pos.X+20, pos.Y + plane1.outH /1.5);
        if (city.isAllBuldingDestroy()){
            this.stop();
        }
    },
    onLoad:() => {
        loadCounter--;
        if (!loadCounter){
            start();
        }
    },
});
const bomb = new Bomb({
    canvasId:'canvas-display-top',
    spriteFileName:'img/bomb1.png',
    onStep:function(pos){
        //const buldingNumber = city.checkContactTopAny( pos );
        const buldingNumber = city.checkContactTopArea( pos );
        if ( buldingNumber!==null ){
            const blowPos = city.getTopAreaCenterBottomPoint(buldingNumber);
            this.stop(true);
            city.removeTopFloor(buldingNumber);
            blows.addSpriteBlow(blowPos.X,blowPos.Y);
            console.log("Bomb contact", pos.Y);
        }
    }
});
bomb.endCallback=()=>{
    plane1.currentSprite = 0;
    if (bomb._spritePos.X > 0 &&  bomb._spritePos.X < bomb.maxW){
        blows.addSpriteBlow((bomb._spritePos.X),(bomb._spritePos.Y-15));
    }
};

function start(){
    document.getElementById("load-progress").parentNode.classList.toggle("d-none");
    city.renderAll();
    plane1.reStart();

    const cnvs = document.getElementById("main-scene");
    document.addEventListener("click", (event) => {
        if (!bomb.isBusy()){
            bomb.reStart(plane1._spritePos.X + 45, plane1._spritePos.Y+30);
            plane1.currentSprite = 1;
        }
        event.preventDefault();
    });
}

console.log( iManager );