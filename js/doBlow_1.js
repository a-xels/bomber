import { Game } from './game.js';

const game = new Game({
    onLoadReady:function(){
        document.getElementById("load-progress").parentNode.classList.toggle("d-none");
        this.city.renderAll();
        this.plane1.reStart();

        const cnvs = document.getElementById("main-scene");
        document.addEventListener("click", (event) => {
            if (!this.bomb.isBusy()){
                this.bomb.reStart(this.plane1._spritePos.X + 45, this.plane1._spritePos.Y+30);
                this.plane1.currentSprite = 1;
            }
            event.preventDefault();
        });

    }
});