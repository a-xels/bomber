/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/javascript.js to edit this template
 */
/* 
 Created on : 29 нояб. 2023 г., 19:55:09
 Author     : Щукин Александр.
 */

import { blowAnimate }from './flame.js';
import { Plane }from './plane.js';
import { Bomb } from './bomb.js';
import { Bilding } from './bilding.js';
import { City } from './city.js';
import { citySprite } from './citysprite.js';
import { ImgManager } from './ImgManager.js';
import { StartPage } from './StartPage.js';

export class Game{
    loadCounter = 0;
    totalLoad = 0;
    city = null;
    firstStart = true;
    frameState = 0;
    onLoadReady;
    blows;
    flame;
    city;
    plane1;
    bomb;
    startPage;
    constructor(options){
        const self = this;
        options = options || {};
        let {
            onLoadReady: _onLoadReady = function(){console.log("Game: loadReady!");}
        } = options;
        this.onLoadReady = _onLoadReady;
        this.iManager = new ImgManager();
        //Стпртовая страница
        //
        this.startPage = new StartPage({
            canvasId : "canvas-display-info",
            scoreListUrl : "/bomber/score.php"
        });
        //Бомба
        const img=new Image();
        img.src="img/blow2.png";
        this.blows=new blowAnimate._blows({
            id:'canvas-display-blow',
            idMidle:'canvas-display-midle',
            img:{
                content:img,
                spriteW:256,
                spriteH:256,
                mult:.8,
                yDivider:1.2
            }
        });
        this.flame=new blowAnimate.flame({
            id:'canvas-display-flame',
            idMidle:'canvas-display-midle'
        });
        this.city = new City({
            canvasId:'canvas-display-back',
            fileNameList:citySprite,
            onImgLoadProgress:this.loadProgressImg,
            iManager:this.iManager,
            onLoad:() => {
                this.loadCounter--;
                if (!this.loadCounter){
                    this.loadsReady();
                }
            }
        });
        this.plane1 = new Plane({
            canvasId:'canvas-display-top',
            spriteFileName:['img/plane2.png', 'img/plane.png'],
            onStepSkipper:2,
            onStep:function(pos){
                self.flame.addFlame(180,pos.X+20, pos.Y + self.plane1.outH /1.5);
                if (self.city.isAllBuldingDestroy()){
                    this.stop();
                }
            },
            onLoad:() => {
                this.loadCounter--;
                if (!this.loadCounter){
                    this.loadsReady();
                }
            }
        });
        this.bomb = new Bomb({
            canvasId:'canvas-display-top',
            spriteFileName:'img/bomb1.png',
            onStep:function(pos){
                //const buldingNumber = city.checkContactTopAny( pos );
                const buldingNumber = self.city.checkContactTopArea( pos );
                if ( buldingNumber!==null ){
                    const blowPos = self.city.getTopAreaCenterBottomPoint(buldingNumber);
                    this.stop(true);
                    self.city.removeTopFloor(buldingNumber);
                    self.blows.addSpriteBlow(blowPos.X,blowPos.Y);
                    console.log("Bomb contact", pos.Y);
                }
            },
            endCallback: () => {
                this.plane1.currentSprite = 0;
                if (this.bomb._spritePos.X > 0 &&  this.bomb._spritePos.X < this.bomb.maxW){
                    this.blows.addSpriteBlow((this.bomb._spritePos.X),(this.bomb._spritePos.Y-15));
                }
            }
        });
        this.makeCyti();
        this.firstStart = false;
    }
    makeCyti(){
        this.loadCounter = 0;
        this.totalLoad = 0;
        this.city.clearPort();
        this.totalLoad = this.city.prepareRender(20, 3);
        this.loadCounter = this.totalLoad + this.firstStart?2:0;//?
    }
    loadProgressImg(){
        const el = document.getElementById("load-progress");
        this.loadCounter--;
        el.setAttribute('max', this.totalLoad);
        el.setAttribute("value", this.totalLoad - this.loadCounter);
        el.textContent = Math.round( ( this.totalLoad - this.loadCounter )/ this.totalLoad * 100) + "%";
    }
    loadsReady(){
        if (typeof this.onLoadReady === 'function'){
            this.onLoadReady.call(this, this.frameState);
        }
        switch(this.frameState){
            case 1:
                //this.newGame;
                break;
            case 2:
                //this.nextLevel();
                break;
            case 3:
                //this.endGame();
                break;
            default:
                this.startPage.run();
                break;
        ``}
    }
}