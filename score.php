<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
ob_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);
if (is_string($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'POST'){
    require_once 'clases/ScoreEng.php';
    ob_clean();
    header('Content-Type: application/json; charset=utf-8');
    echo ( new ScoreEng() )->run();
}else{
    ob_clean();
    header('Content-Type: application/json; charset=utf-8');
    include_once '../errorRequest.php';
}
ob_end_flush ();