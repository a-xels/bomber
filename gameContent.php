<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="scene" id="main-scene">
    <canvas width="1920" height="1080" id="canvas-display-back" class="canvas"></canvas>
    <canvas width="1920" height="1080" id="canvas-display-midle" class="canvas"></canvas>
    <canvas width="1920" height="1080" id="canvas-display-flame" class="canvas"></canvas>
    <canvas width="1920" height="1080" id="canvas-display-blow" class="canvas"></canvas>
    <canvas width="1920" height="1080" id="canvas-display-top" class="canvas"></canvas>
    <canvas width="1920" height="1080" id="canvas-display-info" class="canvas"></canvas>
    <div class="info">
        <label for="load-progress" class="info__label">Загрузка:</label>
        <progress id="load-progress" max="100" value="0" class="info__progress">0%</progress>
    </div>
</div>
<script src="js/doBlow_1.js" type="module"></script>
<!--<script src="js/doBlow.js" type="module"></script>-->
